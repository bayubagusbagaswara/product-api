-- ELECTRONIC - TELEVISION
insert into products (id, name, price, created_by, created_at, status_record)
values ('xiaomi-redmibook-15', 'Xiaomi RedmiBook 15', 6729000, 'Current User', current_timestamp, 'ACTIVE'),
        ('xiaomi-redmibook-pro-15', 'Xiaomi RedmiBook Pro 15', 16499000, 'Current User', current_timestamp, 'ACTIVE'),
        ('samsung-tv-32n4001', 'Samsung TV 32N4001', 2150000, 'Current User', current_timestamp, 'ACTIVE'),
        ('lg-tv-lm550', 'LG TV LM550', 2799000, 'Current User', current_timestamp, 'ACTIVE');

-- ELECTRONIC - AC
insert into products (id, name, price, created_by, created_at, status_record)
values ('sharp-ac-ah-a5ucy', 'Sharp AC AH-A5UCY', 2369000, 'Current User', current_timestamp, 'ACTIVE'),
        ('sharp-ac-ah-a9ucy', 'Sharp AC AH-A9UCY', 2708000, 'Current User', current_timestamp, 'ACTIVE'),
        ('daikin-ac-ftp15', 'Daikin AC FTP15', 2795000, 'Current User', current_timestamp, 'ACTIVE');

-- ELECTRONIC - AUDIO & SPEAKER
insert into products (id, name, price, created_by, created_at, status_record)
values ('polytron-pma-9502', 'Polytron PMA 9502', 517500, 'Current User', current_timestamp, 'ACTIVE'),
        ('polytorn-bb-5510', 'Polytron BB 5510', 2960000, 'Current User', current_timestamp, 'ACTIVE'),
        ('polytron-pma-9505', 'Polytron PMA 9505', 715000, 'Current User', current_timestamp, 'ACTIVE');

-- ELECTRONIC - CCTV
insert into products (id, name, price, created_by, created_at, status_record)
values ('hikvision-ds-2ce5', 'Sharp AC AH-A5UCY', 185000, 'Current User', current_timestamp, 'ACTIVE'),
        ('hikvision-ds-2cd2', 'Hikvision DS-2CD2', 1453000, 'Current User', current_timestamp, 'ACTIVE'),
        ('hikvision-ds-2cd1', 'Hikvision DS-2CD1', 580000, 'Current User', current_timestamp, 'ACTIVE');

-- ELECTRONIC - CAMERA
insert into products (id, name, price, created_by, created_at, status_record)
values ('canon-eos-600d', 'Canon EOS 600D', 4050000, 'Current User', current_timestamp, 'ACTIVE'),
        ('canon-eos-60d', 'Canon EOS 60D', 5100000, 'Current User', current_timestamp, 'ACTIVE'),
        ('sony-alpha', 'Sony Alpha A6000', 5500000, 'Current User', current_timestamp, 'ACTIVE'),
        ('canon-eos-1300d', 'Canon EOS 1300D', 3600000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - LAPTOP
insert into products (id, name, price, created_by, created_at, status_record)
values ('macbook-pro-14-2021', 'Apple MacBook Pro 14-inch 2021', 33390000, 'Current User', current_timestamp, 'ACTIVE'),
        ('macbook-air-m1-2020', 'Apple MacBook Air M1 2020', 12950000, 'Current User', current_timestamp, 'ACTIVE'),
        ('lenovo-legion-5', 'Lenovo Legion 5 Pro', 18199000, 'Current User', current_timestamp, 'ACTIVE'),
        ('lenovo-thinkpad-t430', 'Lenovo ThinkPad T430', 6279000, 'Current User', current_timestamp, 'ACTIVE'),
        ('acer-aspire-3', 'Acer Aspire 3 a314-32', 4937500, 'Current User', current_timestamp, 'ACTIVE'),
        ('hp-pavilion-x360', 'HP Pavilion X360', 7599000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - DISK
insert into products (id, name, price, created_by, created_at, status_record)
values ('seagate-1tb', 'Seagate Expansion 1TB', 590000, 'Current User', current_timestamp, 'ACTIVE'),
        ('sandisk-128gb', 'Sandisk Cruzer 128GB Merah', 63000, 'Current User', current_timestamp, 'ACTIVE'),
        ('seagate-2tb', 'Seagate Expansion 2TB', 600000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - MONITOR
insert into products (id, name, price, created_by, created_at, status_record)
values ('lg-24mk430h', 'LG 24MK430H-B LED Monitor', 1459000, 'Current User', current_timestamp, 'ACTIVE'),
        ('lg-20mk400a', 'LG Monitor LED 20MK400A-B', 875000, 'Current User', current_timestamp, 'ACTIVE'),
        ('lg-24mt48af', 'LG 24MT48AF LED Monitor', 1549000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - PRINTER
insert into products (id, name, price, created_by, created_at, status_record)
values ('epson-l120', 'Epson L120', 1200000, 'Current User', current_timestamp, 'ACTIVE'),
        ('epson-l3110', 'Epson L3110', 1750000, 'Current User', current_timestamp, 'ACTIVE'),
        ('canon-pixma', 'Canon Pixma iP2770', 519000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - KEYBOARD
insert into products (id, name, price, created_by, created_at, status_record)
values ('logitech-k120', 'Logitech K120', 90000, 'Current User', current_timestamp, 'ACTIVE'),
        ('logitech-mk275', 'Logitech MK275', 265000, 'Current User', current_timestamp, 'ACTIVE'),
        ('logitech-k100', 'Logitech K100', 49000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - MOUSE
insert into products (id, name, price, created_by, created_at, status_record)
values ('logitech-m170', 'Logitech M170', 80000, 'Current User', current_timestamp, 'ACTIVE'),
        ('logitech-b100', 'Logitech B100', 42000, 'Current User', current_timestamp, 'ACTIVE'),
        ('logitech-m185', 'Logitech M185', 57000, 'Current User', current_timestamp, 'ACTIVE'),
        ('apple-magic', 'Apple Magic Mouse 2', 1172000, 'Current User', current_timestamp, 'ACTIVE'),
        ('logitech-m331', 'Logitech M331', 106000, 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES - ROUTER
insert into products (id, name, price, created_by, created_at, status_record)
values ('tplink-mr3420', 'TP-LINK MR3420', 295000, 'Current User', current_timestamp, 'ACTIVE'),
        ('tenda-n301', 'Tenda N301', 265000, 'Current User', current_timestamp, 'ACTIVE'),
        ('tplink-wr840n', 'TP-LINK WR840N', 133000, 'Current User', current_timestamp, 'ACTIVE'),
        ('tplink-wr844n', 'TP-LINK WR844N', 139000, 'Current User', current_timestamp, 'ACTIVE');

-- HAND PHONE AND ACCESSORIES - HAND PHONE
insert into products (id, name, price, created_by, created_at, status_record)
values ('samsung-galaxy-z-flip', 'Samsung Galaxy Z Flip', 9350000, 'Current User', current_timestamp, 'ACTIVE'),
        ('apple-iphone-x', 'Apple iPhone X', 7300000, 'Current User', current_timestamp, 'ACTIVE'),
        ('apple-iphone-12-pro-max', 'Apple iPhone 12 Pro Max', 16390000, 'Current User', current_timestamp, 'ACTIVE'),
        ('apple-iphone-11', 'Apple iPhone 11', 10000000, 'Current User', current_timestamp, 'ACTIVE'),
        ('apple-iphone-13-pro-max', 'Apple iPhone 13 Pro Max', 18170000, 'Current User', current_timestamp, 'ACTIVE'),
        ('xiaomi-poco-m3', 'Xiaomi Poco M3', 1850000, 'Current User', current_timestamp, 'ACTIVE'),
        ('xiaomi-redmi-9', 'Xiaomi Redmi 9', 1560000, 'Current User', current_timestamp, 'ACTIVE'),

-- HAND PHONE AND ACCESSORIES - TABLET
insert into products (id, name, price, created_by, created_at, status_record)
values ('samsung-tab-s6', 'Samsung Galaxy Tab S6', 5937000, 'Current User', current_timestamp, 'ACTIVE'),
        ('samsung-tab-a7', 'Samsung Galaxy Tab A7', 2892000, 'Current User', current_timestamp, 'ACTIVE'),
        ('ipad-pro-2021', 'Apple iPad Pro 2021', 12900000, 'Current User', current_timestamp, 'ACTIVE'),
        ('ipad-pro-2020', 'Apple iPad Pro 2020', 12442000, 'Current User', current_timestamp, 'ACTIVE');

-- HAND PHONE AND ACCESSORIES - POWER BANK
insert into products (id, name, price, created_by, created_at, status_record)
values ('mi-10000mah', 'Xiaomi Mi 10000mAh', 75000, 'Current User', current_timestamp, 'ACTIVE'),
        ('redmi-20000mah', 'Xiaomi Redmi 20000mAh', 40000, 'Current User', current_timestamp, 'ACTIVE'),
        ('mofit-10000mah', 'Mofit M11 10000mAh', 85000, 'Current User', current_timestamp, 'ACTIVE');
